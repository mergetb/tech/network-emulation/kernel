// SPDX-License-Identifier: GPL-2.0

/* Copyright (c) 2020 Information Sciences Institute 
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of version 2 of the GNU General Public
 * License as published by the Free Software Foundation.
 *
 * ---
 *
 * This program is a simple two interface forwarding bridge. There is a common
 * memory map that is used by the two umems that are attached to each interface.
 * When packets are received, they are forwarded by acquiring a tx descriptor
 * for the adjacent interface and setting its address and lenght to that of the 
 * rx descriptor. No copying is done to accomplish the forward. The frames
 * within the umem are acquired via a very simple ring-buffer counter function
 * umem_next().
 *
 * The program runs in a loop that first consumes any completions from the
 * kernel, tops off the fill queue and then does any forwarding necessary as 
 * determiend by a polling function.
 *
 * */

#include <linux/bpf.h>
#include <sys/time.h>
#include <sys/resource.h>
#include <getopt.h>
#include <poll.h>
#include <linux/if_xdp.h>
#include <signal.h>
#include <linux/if_link.h>
#include <sys/mman.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>
#include <stdlib.h>
#include <xsk.h>
#include <libbpf.h>
#include <bpf/bpf.h>
#include <errno.h>
#include <string.h>
#include <net/if.h>

typedef __u64 u64;
typedef __u32 u32;


// uncomment for debug output
// #define DEBUG 1

#ifdef DEBUG
#define dbg(...) printf(__VA_ARGS__);
#else
#define dbg(...) do {} while (0);
#endif

#define NUM_DESCS XSK_RING_PROD__DEFAULT_NUM_DESCS
#define BATCH_SIZE 256

static u32 NUM_FRAMES = 524288ul;
static u32 FRAME_SIZE = 2048ul;
static int poll_timeout = 100;
static u32 xdp_flags = XDP_FLAGS_UPDATE_IF_NOEXIST | XDP_FLAGS_DRV_MODE;
static u32 bind_flags = 0;
static int queue = 0;
char *ifx[2];
int ifindex[2];
int prog[2];
struct xsk_socket_info *xsk[2];

struct xsk_umem_info {
        struct xsk_ring_prod fq;
        struct xsk_ring_cons cq;
        struct xsk_umem *umem;
        void *buffer;
};

struct xsk_socket_info {
        struct xsk_ring_cons rx;
        struct xsk_ring_prod tx;
        struct xsk_umem_info *umem;
        struct xsk_socket *xsk;
        unsigned long rx_npkts;
        unsigned long tx_npkts;
        unsigned long prev_rx_npkts;
        unsigned long prev_tx_npkts;
        u32 outstanding_tx;
        u32 outstanding_fq;
        u32 prog_id;
        char *name;
};

static struct option long_options[] = {
        {"skb-mode", no_argument, 0, 's'},
        {"num-frames", required_argument, 0, 'n'},
        {"frame-size", required_argument, 0, 'f'},
        {"zero-copy", no_argument, 0, 'z'},
        {"poll-timeout", required_argument, 0, 'p'},
        {"queue", required_argument, 0, 'q'}
};

static void remove_xdp_program(int i)
{
        u32 curr_prog_id = 0;

        if (bpf_get_link_xdp_id(ifindex[i], &curr_prog_id, xdp_flags)) {
                printf("bpf_get_link_xdp_id failed\n");
                exit(EXIT_FAILURE);
        }
        if (xsk[i]->prog_id == curr_prog_id)
                bpf_set_link_xdp_fd(ifindex[i], -1, xdp_flags);
        else if (!curr_prog_id)
                printf("couldn't find a prog id on a given interface\n");
        else
                printf("program on interface changed, not removing\n");
}

#define exit_with_error(error) \
        __exit_with_error(error, __FILE__, __func__, __LINE__)

static void __exit_with_error(int error, const char *file, const char *func, 
                int line)
{
        fprintf(stderr, "%s:%s:%i: errno: %d \"%s\"\n", 
                        file, func, line, error, strerror(error));
        remove_xdp_program(ifindex[0]);
        remove_xdp_program(ifindex[1]);
        exit(EXIT_FAILURE);
}

static void usage(void)
{
        const char *str = 
                "  Usage: xdpsock_multidev dev0 dev1\n"
                "  Options:\n"
                "  -s, --skb-mode       Use socket buffers\n"
                "  -n, --num-frames=n   Total number of frames (default %d)\n"
                "  -f, --frame-size=n   Set the frame size "
                                        "(must be a power of two," 
                                        "default is %d).\n"
                "  -q, --queue=n        Use queue n (default 0)\n"
                "  -p, --poll=n         Use queue n (default %d ms)\n"
                "  -z, --zero-copy      Zero-copy mode.\n"
                "\n";

        fprintf(stderr, str, NUM_FRAMES, FRAME_SIZE, poll_timeout);
        exit(EXIT_FAILURE);
}


static void parse_command_line(int argc, char **argv)
{
        int option_index, c, ifindex = 0;
        opterr = 0;

        for (;;) {
                c = getopt_long(argc, argv, "sn:p:f:zq:", long_options, 
                                &option_index);
                if (c == -1)
                        break;
                switch (c) {
                        case 's':
                                xdp_flags = XDP_FLAGS_UPDATE_IF_NOEXIST | 
                                            XDP_FLAGS_SKB_MODE;
                                bind_flags |= XDP_COPY;
                                break;
                        case 'n':
                                NUM_FRAMES = atoi(optarg);
                                break;
                        case 'p':
                                poll_timeout = atoi(optarg);
                                break;
                        case 'f':
                                FRAME_SIZE = atoi(optarg);
                                break;
                        case 'z':
                                bind_flags |= XDP_ZEROCOPY;
                                break;
                        case 'q':
                                queue = atoi(optarg);
                                break;
                }
        } 
        if(argc - optind != 2) {
                usage();
        }
        for(size_t i=optind; i<argc; i++) {

                printf("parg %s\n", argv[i]);
                if(ifindex >= 2) {
                        usage();
                }
                ifx[ifindex++] = argv[i];

        }


}

// an extremely simple ring-buffer counter
static inline u64 umem_next(void)
{
        static u64 __i = 0;
        u64 x = __i;
        __i = (__i+1) & (NUM_FRAMES - 1);
        dbg("i=%llu\n", x);
        return x;
}

static struct xsk_umem_info *configure_umem(void *buffer, u64 size)
{
        struct xsk_umem_info *umem;
        struct xsk_umem_config cfg = {
                .fill_size = NUM_DESCS,
                .comp_size = NUM_DESCS,
                .frame_size = FRAME_SIZE,
                .frame_headroom = XSK_UMEM__DEFAULT_FRAME_HEADROOM,
                .flags = 0,
        };
        int ret;

        umem = calloc(1, sizeof(*umem));
        if (!umem) exit_with_error(errno);

        ret = xsk_umem__create(
                        &umem->umem,
                        buffer,
                        size,
                        &umem->fq,
                        &umem->cq,
                        &cfg
                        );

        if (ret) exit_with_error(-ret);

        umem->buffer = buffer;
        return umem;
}

static struct xsk_socket_info *configure_socket(struct xsk_umem_info *umem,
                char *ifx, int ifindex, int queue)
{
        struct xsk_socket_config cfg;
        struct xsk_socket_info *xsk;
        int ret;

        printf("configuring socket %s\n", ifx);

        xsk = calloc(1, sizeof(*xsk));
        if (!xsk) exit_with_error(errno);

        xsk->umem = umem;
        cfg.rx_size = NUM_DESCS;
        cfg.tx_size = NUM_DESCS;
        cfg.bind_flags = bind_flags;
        cfg.xdp_flags = xdp_flags;
        cfg.libbpf_flags = XSK_LIBBPF_FLAGS__INHIBIT_PROG_LOAD;

        ret = xsk_socket__create(&xsk->xsk, ifx, queue, umem->umem, &xsk->rx, 
                                 &xsk->tx, &cfg);
        if (ret) 
                exit_with_error(-ret);

        ret = bpf_get_link_xdp_id(ifindex, &xsk->prog_id, xdp_flags);
        if (ret) 
                exit_with_error(-ret);

        return xsk;
}

static void populate_fill_ring(struct xsk_umem_info *umem)
{
        int ret, i, n = NUM_DESCS;
        u32 idx;

        ret = xsk_ring_prod__reserve(&umem->fq, n, &idx);
        if (ret != n) exit_with_error(-ret);

        for(i=0; i<n; i++) 
                *xsk_ring_prod__fill_addr(&umem->fq, idx++) = 
                        umem_next()*FRAME_SIZE;

        xsk_ring_prod__submit(&umem->fq, n);
}

static struct bpf_object* load_xdp_program(int ifindex)
{
        int ret, fd;
        struct bpf_object *obj;

        struct bpf_prog_load_attr attr = {
                .prog_type = BPF_PROG_TYPE_XDP,
                .file = "xdpsock_multidev_kern.o",
        };

        ret = bpf_prog_load_xattr(&attr, &obj, &fd);
        if (ret) exit_with_error(-ret);
        if (fd < 0) exit_with_error(fd);

        ret = bpf_set_link_xdp_fd(ifindex, fd, xdp_flags);
        if (ret < 0) exit_with_error(-ret);

        return obj;
}

static void enter_xsk_into_map(struct xsk_socket_info *xsk, 
                struct bpf_object *obj, int queue)
{
        struct bpf_map *map;
        int xsks_map;
        int fd = xsk_socket__fd(xsk->xsk);
        int ret;

        printf("xsk_map: %s %d -> %d\n", xsk->name, queue, fd);

        map = bpf_object__find_map_by_name(obj, "xsks_map");
        xsks_map = bpf_map__fd(map);
        if (xsks_map < 0) exit_with_error(xsks_map);

        ret = bpf_map_update_elem(xsks_map, &queue, &fd, 0);
        if (ret) exit_with_error(-ret);
}

static void kick_tx(struct xsk_socket_info *xsk)
{
        int ret;

        ret = sendto(xsk_socket__fd(xsk->xsk), NULL, 0, MSG_DONTWAIT, NULL, 0);
        if (ret >= 0 || errno == ENOBUFS || errno == EAGAIN || errno == EBUSY)
                return;
        exit_with_error(errno);
}

// replenish fill queue
static void fq_replenish(struct xsk_socket_info *xsk, struct pollfd *fds)
{
        u32 idx_fq = 0;
        size_t ndescs;
        int ret, i;

        if(!xsk->outstanding_fq) 
                return;

        ndescs = xsk->outstanding_fq < BATCH_SIZE 
                        ? xsk->outstanding_fq 
                        : BATCH_SIZE;

        ret = xsk_ring_prod__reserve(&xsk->umem->fq, ndescs, &idx_fq);
        if (ret < 0) 
                exit_with_error(-ret);
        else if (ret == 0)
                return;

        dbg("fq: replenish %d\n", ret);
        if (xsk_ring_prod__needs_wakeup(&xsk->umem->fq)) 
                poll(fds, 2, 0);
        for (i = 0; i < ret; i++)
                *xsk_ring_prod__fill_addr(&xsk->umem->fq, idx_fq++) = 
                        umem_next()*FRAME_SIZE;

        xsk_ring_prod__submit(&xsk->umem->fq, ret);
        xsk->outstanding_fq -= ret;
}

// consume completion queue
static void tx_complete(struct xsk_socket_info *xsk)
{
        u32 idx_cq = 0;
        size_t ndescs;
        unsigned int rcvd;

        if (!xsk->outstanding_tx) 
                return;

        if (xsk_ring_prod__needs_wakeup(&xsk->tx)) 
                kick_tx(xsk);

        ndescs = xsk->outstanding_tx < BATCH_SIZE 
                 ? xsk->outstanding_tx 
                 : BATCH_SIZE;

        rcvd = xsk_ring_cons__peek(&xsk->umem->cq, ndescs, &idx_cq);
        if (rcvd <= 0) {
                dbg("tx: nothing transmitted\n");
                return;
        }

        dbg("tx: %d packets transmitted out %s\n", rcvd, xsk->name);
        xsk_ring_cons__release(&xsk->umem->cq, rcvd);
        xsk->outstanding_tx -= rcvd;
        xsk->tx_npkts += rcvd;
}

// forward packets across devices
static void fwd(struct xsk_socket_info *from, 
                struct xsk_socket_info *to, struct pollfd *fds)
{
        unsigned int rcvd, i;
        u32 idx_rx = 0, idx_tx = 0;
        int ret;

        // check if there are frames to grab

        rcvd = xsk_ring_cons__peek(&from->rx, BATCH_SIZE, &idx_rx);
        if (rcvd < 0) 
                exit_with_error(rcvd);
        else if (rcvd == 0)
                return;

        dbg("fwd: forwarding a packet %s -> %s\n", from->name, to->name);

        // reserve transmission capacity

        ret = xsk_ring_prod__reserve(&to->tx, rcvd, &idx_tx);
        while (ret != rcvd) {
                printf("fwd: re-reserve\n");
                if (ret < 0) exit_with_error(-ret);
                if (xsk_ring_prod__needs_wakeup(&to->tx)) kick_tx(to);
                ret = xsk_ring_prod__reserve(&to->tx, rcvd, &idx_tx);
        }

        // forward rx descriptors to tx descriptors

        for (i = 0; i < rcvd; i++) {
                const struct xdp_desc *in;
                struct xdp_desc *out;

                // collect ingress/egress descriptors
                in = xsk_ring_cons__rx_desc(&from->rx, idx_rx++);
                out = xsk_ring_prod__tx_desc(&to->tx, idx_tx++);

                dbg("fwd: addr=%lld len=%d\n", in->addr, in->len);

                // apply ingres as egress (forward)
                out->addr = in->addr;
                out->len  = in->len;
        }

        // submit forwarded descriptors to tx ring
        xsk_ring_prod__submit(&to->tx, rcvd);
        // done with ingress descriptors
        xsk_ring_cons__release(&from->rx, rcvd);


        // stats
        from->rx_npkts += rcvd;
        from->outstanding_fq += rcvd;
        to->outstanding_tx += rcvd;
}

static void forward(struct xsk_socket_info *a, struct xsk_socket_info *b)
{

        int ret;
        struct pollfd fds[2] = {
                { .fd = xsk_socket__fd(a->xsk), .events = POLLIN },
                { .fd = xsk_socket__fd(b->xsk), .events = POLLIN }
        };

        for (;;) {
                tx_complete(a);
                tx_complete(b);

                fq_replenish(a, fds);
                fq_replenish(b, fds);

                ret = poll(fds, 2,  poll_timeout);
                if (ret <= 0) continue;
                if (fds[0].revents) fwd(a, b, fds);
                if (fds[1].revents) fwd(b, a, fds);
        }

}

static void int_exit(int sig)
{
        struct xsk_umem *umem = xsk[0]->umem->umem;
        int i;

        for (i = 0; i < 2; i++)
                xsk_socket__delete(xsk[i]->xsk);
        (void)xsk_umem__delete(umem);
        remove_xdp_program(0);
        remove_xdp_program(1);

        exit(EXIT_SUCCESS);
}

int main(int argc, char **argv)
{
        struct rlimit r = {RLIM_INFINITY, RLIM_INFINITY};
        void *buf;
        struct xsk_umem_info *umem0, *umem1;
        struct bpf_object *prog0, *prog1;

        parse_command_line(argc, argv);

        if (setrlimit(RLIMIT_MEMLOCK, &r)) {
                fprintf(stderr, "ERROR: setrlimit(RLIMIT_MEMLOCK) \"%s\"\n",
                                strerror(errno));
                exit(EXIT_FAILURE);
        }


        ifindex[0] = if_nametoindex(ifx[0]);
        if (!ifindex[0]) {
                fprintf(stderr, "ERROR: device '%s' does not exist\n", ifx[0]);
                exit(EXIT_FAILURE);
        }

        ifindex[1] = if_nametoindex(ifx[1]);
        if (!ifindex[1]) {
                fprintf(stderr, "ERROR: device '%s' does not exist\n", ifx[1]);
                exit(EXIT_FAILURE);
        }

        printf("loading program 1\n");
        prog0 = load_xdp_program(ifindex[0]);
        printf("loading program 2\n");
        prog1 = load_xdp_program(ifindex[1]);

        // reserve memory for UMEM
        buf = mmap( NULL, 
                        NUM_FRAMES * FRAME_SIZE, 
                        PROT_READ | PROT_WRITE,
                        MAP_PRIVATE | MAP_ANONYMOUS,
                        -1,
                        0
                  );
        if (buf == MAP_FAILED) {
                printf("ERROR: mmap failed\n");
                exit(EXIT_FAILURE);
        }

        umem0 = configure_umem(buf, NUM_FRAMES * FRAME_SIZE);
        umem1 = configure_umem(buf, NUM_FRAMES * FRAME_SIZE);
        populate_fill_ring(umem0);
        populate_fill_ring(umem1);

        xsk[0] = configure_socket(umem0, ifx[0], ifindex[0], queue);
        xsk[0]->name = ifx[0];
        xsk[1] = configure_socket(umem1, ifx[1], ifindex[1], queue);
        xsk[1]->name = ifx[1];

        enter_xsk_into_map(xsk[0], prog0, queue);
        enter_xsk_into_map(xsk[1], prog1, queue);

        signal(SIGINT, int_exit);
        signal(SIGTERM, int_exit);
        signal(SIGABRT, int_exit);


        forward(xsk[0], xsk[1]);
}
